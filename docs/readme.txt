
Project:
	-IDE: Elipse Mars
	-JDK: 1.8
	--Maven (4.0) project
	--dependencies:
	  * Spring Framework 4.1.6.RELEASE
	  * log4j 1.2.17
	  * Junit 4.12
	
	--AppMain:  Main class, check args and lauch ElevatorService
	--ElevatorService: 
		1) load CommandSetDO list from file
		2) get instance of CommandSetManager and ElevatorController
		3) execute and get results
	--CommandSetManager: getNext() command data object (CommandSetDO)
	--ElevatorController: 
		1)instance Dispatcher based on MODE argument,DispatcherA or DispatcherB
		2)process CommandSetDO and return result steps/stops
	--Dispatcher: --determine next stop for the elevator
		DispatcherA: for Mode A requirement - inefficient way
		DispatcherB: for Mode B requirements - efficient operation
		
	--resources: 
		1)applicationContext.xml for beans defined
		2)log4j.properties (for both console and file output)
		3)test files are under /src/test/resources folder
		4)log files are under log folder
		

Ways to run the program:
	1) Run Maven build to download all dependencies
	2) There are different ways to run the progam:
		a) Run as java application set up
			>From Eclipse, choose Run...Run Configuration...
			>Double click Java Applcation, fill gfx.AppMain as class
			>Fill Progam arguments:
				for A mode: ./src/test/resources/test.txt A
				For B mode: ./src/test/resources/test.txt B
			>Click Apply and Run

		2)Run Maven build:
			>From Eclipse, choose Run...Run Configuration...
			>Double click Maven build, fill:
				name: elevator
				Browse and fill base directory: ${workspace_loc:/elevator}
				add parameters: 
					Goals: exec:java
					exec.mainClass: gfx.AppMain
					exec.args: ./src/test/resources/test.txt A
						(or B for mode B)
	

Here are my test results:

***************************************************
A_MODE Results:

10  8  1 (9)
 1  5  1  6  1  5 (30)
 4  1  4  2  6  8 (16)
 7  9  3  7  5  8  7  11  11  1 (36)
 11  6  10  5  6  8  7  4  12  7  8  9 (40)
 1  8  6  8 (16)

***************************************************	

***************************************************
B_MODE Results:

10  8  1 (9)
 1  5  6 (13)
 4  2  1  6  8 (12)
 5  7  8  9  11  11  1 (18)
 11  10  6  5  6  8  12  7  4  8  9 (30)
 1  6  8 (12)

***************************************************


Unfinished work:
Due to my scheduled activities, I mainly focused on solution. 
Things can be improved:
1) DispatcherModeB needs refactored
2) Add more tests
3) Document 


