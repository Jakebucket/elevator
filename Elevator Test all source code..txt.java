
Project:
	-IDE: Elipse Mars
	-JDK: 1.8
	--Maven (4.0) project
	--dependencies:
	--Spring Framework 4.1.6.RELEASE
	--log4j 1.2.17
	
�	AppMain:  Main class, check args and lauch ElevatorService
	--ElevatorService: 
		1) load CommandSetDO list from file
		2) get instance of CommandSetManager and ElevatorController
		3) execute and get results
�	CommandSetManager: getNext() command data object (CommandSetDO)
�	ElevatorController: 
		1)instance Dispatcher based on MODE argument,DispatcherA or DispatcherB
		2)process CommandSetDO and return result steps/stops
�	Dispatcher: --determine next stop for the elevator
		DispatcherA: for Mode A requirement - inefficient way
		DispatcherB: for Mode B requirements - efficient operation
		
�	resources: 
		1)applicationContext.xml for beans defined
		2)log4j.properties (for both console and file output)
		3)test files are under /src/test/resources folder
		4)log files are under log folder
		

Ways to run the program:
	1) Run Maven build to download all dependencies
	2) There are different ways to run the progam:
		a) Run as java application set up
			>From Eclipse, choose Run...Run Configuration...
			>Double click Java Applcation, fill gfx.AppMain as class
			>Fill Progam arguments:
				for A mode: ./src/test/resources/test.txt A
				For B mode: ./src/test/resources/test.txt B
			>Click Apply and Run

		2)Run Maven build:
			>From Eclipse, choose Run...Run Configuration...
			>Double click Maven build, fill:
				name: elevator
				Browse and fill base directory: ${workspace_loc:/elevator}
				add parameters: 
					Goals: exec:java
					exec.mainClass: gfx.AppMain
					exec.args: ./src/test/resources/test.txt A
						(or B for mode B)
	

Test input :

10:8-1
9:1-5,1-6,1-5
2:4-1,4-2,6-8
3:7-9,3-7,5-8,7-11,11-1
7:11-6,10-5,6-8,7-4,12-7,8-9
6:1-8,6-8


Test outputs:

***************************************************
A_MODE Results:

10  8  1 (9)
 1  5  1  6  1  5 (30)
 4  1  4  2  6  8 (16)
 7  9  3  7  5  8  7  11  11  1 (36)
 11  6  10  5  6  8  7  4  12  7  8  9 (40)
 1  8  6  8 (16)

***************************************************	

***************************************************
B_MODE Results:

10  8  1 (9)
 1  5  6 (13)
 4  2  1  6  8 (12)
 5  7  8  9  11  11  1 (18)
 11  10  6  5  6  8  12  7  4  8  9 (30)
 1  6  8 (12)

***************************************************
Pom.xml:
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>gfx.elevator</groupId>
	<artifactId>elevator</artifactId>
	<version>1.0-SNAPSHOT</version>
	<name>GFX Elevator</name>
	<description>GFX Elevator Problem Solution</description>
	<dependencies>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j.version}</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.4.0</version>
				<executions>
					<execution>
						<phase>test</phase>
						<goals>
							<goal>java</goal>
						</goals>
						<configuration>
							<mainClass>gfx.elevator.AppMain</mainClass>
							<arguments>
								<argument>./src/test/resources/commands.txt</argument>
								<argument>B</argument>
							</arguments>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	
 
	<properties>
		<spring.version>4.1.6.RELEASE</spring.version>
		<junit.version>4.12</junit.version>
		<log4j.version>1.2.17</log4j.version>
	</properties>	

</project>
.project:
<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
	<name>elevator</name>
	<comment></comment>
	<projects>
	</projects>
	<buildSpec>
		<buildCommand>
			<name>org.eclipse.jdt.core.javabuilder</name>
			<arguments>
			</arguments>
		</buildCommand>
		<buildCommand>
			<name>org.eclipse.m2e.core.maven2Builder</name>
			<arguments>
			</arguments>
		</buildCommand>
	</buildSpec>
	<natures>
		<nature>org.eclipse.jdt.core.javanature</nature>
		<nature>org.eclipse.m2e.core.maven2Nature</nature>
	</natures>
</projectDescription>

.classpath:
<?xml version="1.0" encoding="UTF-8"?>
<classpath>
	<classpathentry kind="src" output="target/classes" path="src/main/java">
		<attributes>
			<attribute name="optional" value="true"/>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry excluding="**" kind="src" output="target/classes" path="src/main/resources">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="src" output="target/test-classes" path="src/test/java">
		<attributes>
			<attribute name="optional" value="true"/>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry excluding="**" kind="src" output="target/test-classes" path="src/test/resources">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="con" path="org.eclipse.m2e.MAVEN2_CLASSPATH_CONTAINER">
		<attributes>
			<attribute name="maven.pomderived" value="true"/>
		</attributes>
	</classpathentry>
	<classpathentry kind="output" path="target/classes"/>
</classpath>

AppMain.java
package gfx;

import org.apache.log4j.Logger;
import gfx.elevator.ModeType;

public class AppMain {
	private final static Logger logger = Logger.getLogger(AppMain.class);
	
	
	public static void main(String[] args) {
		if (validate(args)){
			ElevatorService service = (ElevatorService) AppContext.getContext().getBean("elevatorService");
			service.process(args[0], ModeType.getMode(args[1]));
		}else{
			System.exit(1);
		}
	}
	
	private static boolean validate(String[] args){
		if (args.length != 2) {
			logger.error("Please input two parameters to run the program: fileName and mode");
			return false;
		}else if (ModeType.getMode(args[1]).equals(ModeType.UNKNOWN)){
			logger.error("Un-defined mode: " + args[1]);
			return false;
		}else{
			return true;
		}
		
	}
}

package gfx;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppContext {
	private static ApplicationContext context = null;
	private AppContext() {
	}
	
	static {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
	}
	
	public static ApplicationContext getContext(){
		return context;
	}
}

package gfx;

import gfx.elevator.ModeType;

public interface ElevatorService {
	void process(String fileName, ModeType mode);
}


package gfx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import gfx.command.CommandSetDO;
import gfx.command.CommandSetManager;
import gfx.command.CommandSetParser;
import gfx.elevator.ElevatorController;
import gfx.elevator.ModeType;

@Service("elevatorService")
public class ElevatorServiceImpl implements ElevatorService{
	private final static Logger logger = Logger.getLogger(ElevatorServiceImpl.class);
	private String fileName; 
	private ModeType mode;
	
	public ElevatorServiceImpl() {
	}
	public String getFileName() {
		return fileName;
	}
	public ModeType getMode() {
		return mode;
	}
	
	public void process(String fileName, ModeType mode) {
		this.fileName = fileName;
		this.mode = mode;
		
		try {
			//parse commands file
			List<CommandSetDO> commandSetList = CommandSetParser.parse(fileName);
			//prepare command set manager/controller to feed commands
			CommandSetManager commandSetManager = (CommandSetManager) AppContext.getContext().getBean("commandSetManager");
			commandSetManager.setCommandSetList(commandSetList);
			//prepare elevator controller
			ElevatorController elevatorController = (ElevatorController)AppContext.getContext().getBean("elevatorController");
			elevatorController.setMode(mode);
			//process commands and record results
			List<String> results = execute(commandSetManager,elevatorController );
			
			//display results
			showResults(results);
			
		} catch (AppException ex) {
			logger.error("process() failed to parse fileName: " + fileName, ex);
		}
	}
	
	private List<String> execute(CommandSetManager commandSetManager, ElevatorController elevatorController) {
		List<String> results = new ArrayList<String>();
		CommandSetDO commandSetDO = commandSetManager.getNext();
		StringBuilder sb = new StringBuilder();
		sb.append(commandSetDO.getPosition() + " ");
		while (commandSetDO != null) {
			try {
				Integer[] steps = elevatorController.process(commandSetDO);
				int totalDistance = elevatorController.getTotalDistance();
				//sb.append(Arrays.toString(steps));
				sb.append(convertArrayToString(steps));
				sb.append( "(" + totalDistance + ")\n");
				
				commandSetDO = commandSetManager.getNext();
			} catch (AppException ex) {
				sb.append(commandSetDO.toStr() + "(failed)\n");
				logger.error("execute() failed to process commandSetDO: " + commandSetDO.toStr(), ex);
			}
		}
		results.add(sb.toString());
		return results;
	}
	
	private String convertArrayToString(Integer[] steps) {
		return (Arrays.toString(steps)).replaceAll("[\\[,\\]]", " ");
	}
	
	private void showResults(List<String> results) {
		String markLine = "***************************************************\n";
		StringBuilder display = new StringBuilder();
		display.append("\n" +  markLine);
		display.append(mode  + " Results:\n\n");
		for (int i=0;i<results.size();i++){
			display.append(results.get(i) + "\n");
		}
		display.append(markLine);
		
		if (logger.isInfoEnabled()){
			logger.info(display.toString());		
		}else{
			System.out.println(display.toString());		
		}
	}

}


public class AppException extends Exception {

	public AppException() {
	}

	public AppException(String message) {
		super(message);
	}

	public AppException(Throwable cause) {
		super(cause);
	}

	public AppException(String message, Throwable cause) {
		super(message, cause);
	}

	public AppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}


package gfx.elevator;

public interface Elevator {
	void initialize(int position);
	int getPosition();
	int getTotalDistance();
	void moveTo(int nextStop);
}


package gfx.elevator;

public class ElevatorImpl implements Elevator {
	private int position;
	private int totalDistance;
	
	public ElevatorImpl() {
		
	}

	public int getPosition() {
		return position;
	}

	public void initialize(int position) {
		this.position = position;
		this.totalDistance = 0;
	}

	public int getTotalDistance() {
		return totalDistance;
	}

	public void moveTo(int nextStop) {
		totalDistance += Math.abs(position - nextStop);
		position = nextStop;
	}

	

}



package gfx.elevator;

import gfx.AppException;
import gfx.command.CommandSetDO;

public interface ElevatorController {
	void setMode(ModeType mode);
	Integer[] process(CommandSetDO commandSetDO) throws AppException;
	int getTotalDistance();
}


package gfx.elevator;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import gfx.AppContext;
import gfx.AppException;
import gfx.command.CommandSetDO;
import gfx.command.Dispatcher;

public class ElevatorControllerImpl implements ElevatorController{
	private final static Logger logger = Logger.getLogger(ElevatorControllerImpl.class);
	private ModeType mode;
	private int totalDistance;
	public ElevatorControllerImpl() {
		// TODO Auto-generated constructor stub
	}

	public void setMode(ModeType mode) {
		this.mode = mode;
	}

	/**
	 * Dispatcher find next stop from CommandSetDO, and
	 * Tell Elevator to move to next stop.
	 * After finished, get total distance from elevator
	 * @return result Integer[] 
	 * @throws AppException
	 */
	public Integer[] process(CommandSetDO commandSetDO) throws AppException {
		if (validate(commandSetDO)){
			List <Integer> stepsList =new ArrayList<Integer>(); 
			logger.debug("process commandSetDO: " + commandSetDO.toString());
			Elevator elevator = (Elevator) AppContext.getContext().getBean("elevator");
			elevator.initialize(commandSetDO.getPosition());
			Dispatcher dispatcher = getDispatcher();
			dispatcher.doCommandSetDO(commandSetDO);
			int nextStop = dispatcher.getNextStop();
			while(nextStop > 0 ) {
				stepsList.add(nextStop);
				elevator.moveTo(nextStop);
				nextStop = dispatcher.getNextStop();
			}
			totalDistance = elevator.getTotalDistance();
			
			return stepsList.toArray(new Integer[stepsList.size()]);
		}else{
			String err = "invalid CommandSetDO to process.";
			logger.error(err);
			throw new AppException(err);
		}
	}
	
	/**
	 * There are to dispatchers based on mode A or B
	 * @return Dispatcher
	 * @throws AppException
	 */
	private Dispatcher getDispatcher() throws AppException{
		if (mode ==ModeType.A_MODE){
			return (Dispatcher) AppContext.getContext().getBean("dispatcherModeA");
		}else if (mode ==ModeType.B_MODE){
			return (Dispatcher) AppContext.getContext().getBean("dispatcherModeB");
		}else{
			throw new AppException("Unknow mode to load dispatcher.");
		}
	}

	private boolean validate(CommandSetDO commandSetDO){
		return (commandSetDO != null 
				&& commandSetDO.getPosition() > 0
				&& commandSetDO.getCommandList().size() > 0);
	}

	public int getTotalDistance() {
		return totalDistance;
	}

}



package gfx.elevator;

public enum ModeType {
	A_MODE("A"),
	B_MODE("B"),
	UNKNOWN("X");
	
	private String mode;
	
	private ModeType(String mode){
		this.mode = mode;
	}
	
	public String getName(){
		return mode;
	}
	
	public static ModeType getMode(String mode){
		if (A_MODE.getName().equals(mode)){
			return A_MODE;
		}else if (B_MODE.getName().equals(mode)){
			return B_MODE;
		}else{
			return UNKNOWN;
		}
	}
}


package gfx.command;

public class CommandBean {
	private int startFloor;
	private int endFloor;
	private boolean moveUp;
	private CommandStateType state = CommandStateType.WAITING;
	
	public CommandBean(){}
	
	public CommandBean(int start, int end){
		this.startFloor = start;
		this.endFloor = end;
		this.moveUp = (end > start);
	}
	
	public int getStartFloor() {
		return startFloor;
	}
	
	public void setStartFloor(int startFloor) {
		this.startFloor = startFloor;
	}
	public int getEndFloor() {
		return endFloor;
	}
	public void setEndFloor(int endFloor) {
		this.endFloor = endFloor;
	}
	public boolean isMoveUp() {
		return moveUp;
	}
	public void setMoveUp(boolean moveUp) {
		this.moveUp = moveUp;
	}

	public CommandStateType getState() {
		return state;
	}

	public void setState(CommandStateType state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CommandBean [startFloor=" + startFloor + ", endFloor=" + endFloor + ", moveUp=" + moveUp + ", state="
				+ state + "]";
	}
	
}


package gfx.command;

import java.util.ArrayList;
import java.util.List;

public class CommandSetDO {
	private int position;
	private List<CommandBean> commandList = new ArrayList<CommandBean>();
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public List<CommandBean> getCommandList() {
		return commandList;
	}
	public void setCommandList(List<CommandBean> commandList) {
		this.commandList = commandList;
	}
	
	/**
	 * validate command data 
	 * @return
	 */
	public boolean validate(){
		return (position > 0 && commandList.size() > 0);
	}
	
	/**
	 * format command set for output
	 * @return
	 */
	public String toStr(){
		StringBuilder sb = new StringBuilder();
		sb.append(position);
		sb.append(" ");
		for (int i=0;i<commandList.size();i++){
			sb.append(commandList.get(i).getStartFloor());
			sb.append(" ");
			sb.append(commandList.get(i).getEndFloor());
			sb.append(" ");
		}
		
		return sb.toString();
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CommandSetDO \n[position=" + position);
		sb.append(", commandList=");
		for (int i=0;i<commandList.size();i++){
			sb.append("(");
			sb.append(commandList.get(i).getStartFloor() + ", ");
			sb.append(commandList.get(i).getEndFloor());
			sb.append(") ");
		}
		sb.append("]");
		return sb.toString();
	}
	
	
}


package gfx.command;

import java.util.List;

public interface CommandSetManager {
	void setCommandSetList(List<CommandSetDO> commandSetList);
	CommandSetDO getNext();
}


package gfx.command;

import java.util.ArrayList;
import java.util.List;
/**
 * provide next CommandSetDO available 
 * @author Jake
 *
 */

public class CommandSetManagerImpl implements CommandSetManager{
	private List<CommandSetDO> commandSetList = new ArrayList<CommandSetDO>();
	private int index; //keep pointer for next CommandSetDO

	public void setCommandSetList(List<CommandSetDO> commandSetList) {
		this.commandSetList = commandSetList;
	}
	
	public CommandSetDO getNext(){
		if (index < commandSetList.size()){
			return commandSetList.get(index++);
		}else{
			return null;//no more left
		}
	}
	
}


package gfx.command;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import gfx.AppException;

/**
 * Util class - parse input file to build CommandSet list
 * @author Jake
 *
 */
public class CommandSetParser {
	private final static Logger logger = Logger.getLogger(CommandSetParser.class);
		
	public static List<CommandSetDO> parse(String fileName)throws AppException{
		List<CommandSetDO> commandSetList = new ArrayList<CommandSetDO>();
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine();
			while (line != null) {
				commandSetList.add(parseLine(line));
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			String err = "Failed to find the file: " + fileName;
			logger.error(err + e.getMessage());
			throw new AppException(e);
		} catch (IOException e) {
			String err = "Failed to read the file: " + fileName;
			logger.error(err + e.getMessage());
			throw new AppException(e);
		}finally{
			if (reader !=null){
				try {
					reader.close();
				} catch (IOException e) {
					logger.warn("Unable to close the file " + fileName + e.getMessage());				
				}
			}
		}
		
		return commandSetList;
	}

	private static CommandSetDO parseLine(String line) {
		CommandSetDO commandSet = null; 
		String [] data = line.split(":");
		if (data.length == 2){
			commandSet = new CommandSetDO();
			commandSet.setPosition(Integer.parseInt(data[0]));
			String[] commands = data[1].split(",");
			if (commands.length > 0) {
				for (int i=0;i <commands.length;i++){
					String[] cmd = commands[i].split("-");
					if (cmd.length == 2){
						commandSet.getCommandList().add(
								new CommandBean(Integer.parseInt(cmd[0]), Integer.parseInt(cmd[1])));
					}else{
						logger.warn("Unable to parse command: " + commands[i]);
					}
				}
			}else{
				logger.warn("Unable to parse data commands: " + data[1]);
			}
		}else{
			logger.warn("Unable to parse data line: " + line);
		}
		return commandSet;
	}
	
	
}


package gfx.command;

public enum CommandStateType {
	WAITING(-1),
	LOADED(1),
	DONE(0);
	
	private int value;
	private CommandStateType(int value){
		this.value = value;
	}
}


package gfx.command;

import gfx.AppException;

public interface Dispatcher {
	void doCommandSetDO(CommandSetDO commandSet) throws AppException;
	int getNextStop();
}


package gfx.command;

import java.util.List;

import org.apache.log4j.Logger;

public class DispatcherModeA implements Dispatcher{
	private final static Logger logger = Logger.getLogger(DispatcherModeA.class);	
	private int elevatorInitPostion;
	private List<CommandBean>commandBeanList;
	private int commandBeanIndex = -1;
	private int stopAt = -1;
	public DispatcherModeA()  {
		// TODO Auto-generated constructor stub
	}

	public void doCommandSetDO(CommandSetDO commandSetDO) {
		commandBeanList = commandSetDO.getCommandList();
		elevatorInitPostion = commandSetDO.getPosition();
		commandBeanIndex = 0;
	}

	public int getNextStop() {
		if (commandBeanIndex <commandBeanList.size()){
			CommandBean cmd = commandBeanList.get(commandBeanIndex);
			if (stopAt == -1 ){//first
				stopAt = cmd.getStartFloor();
			}else if (stopAt == cmd.getStartFloor()){//start one cmd. move to start
				stopAt = cmd.getEndFloor();
			}else if (stopAt == cmd.getEndFloor()){//finish one cmd, move to next
				commandBeanIndex++;
				if (commandBeanIndex <commandBeanList.size()){
					cmd = commandBeanList.get(commandBeanIndex);
					stopAt = cmd.getStartFloor();
				}else{
					stopAt = -1;
				}
			}
		}else{//done
			stopAt = -1;
		}
		return stopAt;
	}

}


package gfx.command;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import gfx.AppException;
import static gfx.command.CommandStateType.*;
/**
 * Mode B method to detemine where elevator to stop next. -1 means done
 * @author Jake
 *
 */
public class DispatcherModeB implements Dispatcher{
	private final static Logger logger = Logger.getLogger(DispatcherModeB.class);	
	private List<CommandBean>commandBeanList;
	private int elevatorInitPostion;
	//stopAt - for getNextStop
	private int stopAt = -1;
	//working vars - better be defined as a separate class
	private Integer [] upStops;// unique upstops
	private Integer [] downStops; // unique downstops
	private int upIndex = -1;
	private int downIndex = -1;
	//local counters and flags
	private boolean boolStarted = false;
	private boolean boolGoUp = false;
	private boolean changeDirection = false;
	private int headCount = 0;
	
	public DispatcherModeB()  {
		// TODO Auto-generated constructor stub
	}

	public void doCommandSetDO(CommandSetDO commandSetDO){
		elevatorInitPostion = commandSetDO.getPosition(); 
		commandBeanList = commandSetDO.getCommandList();
		initialize();
	}
	
	/**
	 * find next stop -1 means done
	 */
	public int getNextStop() {
		if (!boolStarted){//first time
			setFirstStopAt();
		}else{//already started operation
			if (boolGoUp){
				if (headCount > 0){// continue up
					changeDirection = false;
					setUpStopAtNext();
				}else{//elevator is empty now. where to go
					setEmptyStopAtNext();
				}
			}else{//down
				if (headCount > 0){// continue DOWN
					changeDirection = false;
					setDownStopAtNext();
				}else{//Empty now
					setEmptyStopAtNext();
				}
			}
		}
		trackHeadCount();
		return stopAt;
	}
	
	/**
	 * first stop is based on startFloor for the first commandBean/pair
	 */
	private void setFirstStopAt() {
		CommandBean commandBean = commandBeanList.get(0);
		stopAt = commandBean.getStartFloor();
		boolGoUp = commandBean.isMoveUp();
		boolean elevGoUp = (commandBean.getStartFloor() > elevatorInitPostion);
		boolStarted = true;
		trackInitCount(elevGoUp);
		//any stop needed between

		if (boolGoUp && elevGoUp){
			boolean found = false;
			for (int n=0; n<upStops.length && !found; n++){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (cmd.isMoveUp() 
							&& (cmd.getStartFloor() == upStops[n] && cmd.getState() == WAITING)) {
						stopAt = upStops[n];
						upIndex = n;
						found = true;
						break;
					}
				}
			}
		}else if (!boolGoUp && !elevGoUp){//down
			boolean found = false;
			for (int n= (downStops.length -1); n >= 0 && !found; n--){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (!cmd.isMoveUp() 
							&& (cmd.getStartFloor() == downStops[n] && cmd.getState() == WAITING)) {
						stopAt = downStops[n];
						downIndex = n;
						found = true;
						break;
					}
				}
			}			
		}
		
	}

	private void setEmptyStopAtNext() {
		if (upIndex < upStops.length && downIndex > 0) {
			//boolean nextGoUp = (upStops[upIndex] > downStops[downIndex]);
			if ((upStops[upIndex] == downStops[downIndex])){
				boolGoUp = !boolGoUp; //just change direction
			}else {
				//who is still waiting 
				CommandBean waitCmd = findNextWait();
				if (waitCmd != null) {
					//boolean nextGoUp = (upStops[upIndex] > downStops[downIndex]);
					boolean nextGoUp =(waitCmd.getStartFloor() > stopAt);
					if (nextGoUp == boolGoUp){//same direction
						changeDirection = false;
						if (boolGoUp){
							setUpStopAtNext();
						}else{
							setDownStopAtNext();
						}
					}else{//change direction - counter no move
						changeDirection = true;
						if (nextGoUp) {
							setUpStopAtNext();
						}else{
							setDownStopAtNext();
						}
						boolGoUp = nextGoUp;
					}					
				}else {//no more
					stopAt = -1;
				}
			}
		}else if (upIndex < upStops.length){
			changeDirection = !boolGoUp;
			boolGoUp = true;
			upIndex = 0;
			setUpStopAtNext();
		}else if ( downIndex > 0){
			changeDirection = !boolGoUp;
			boolGoUp = false;
			downIndex = upStops.length -1;
			setDownStopAtNext();
		}else{
			stopAt = -1;
		}
		
	}

	private CommandBean findNextWait() {
		for(int i=0;i<commandBeanList.size();i++){
			if (commandBeanList.get(i).getState() == WAITING){
				return commandBeanList.get(i);
			}
		}
		return null;
	}
	private int findNextWaitIndex() {
		for(int index=0;index <commandBeanList.size();index++){
			if (commandBeanList.get(index).getState() == WAITING){
				return index;
			}
		}
		return -1;
	}

	private void setDownStopAtNext() {
		boolean found = false;
		if (downIndex >= 0){
			for (int n= downStops.length-1; n >= 0 && !found; n--){

				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (!cmd.isMoveUp()){
						if ((cmd.getEndFloor() == downStops[n] && cmd.getState() == LOADED)){
							stopAt = downStops[n];
							downIndex = n;
							found = true;
							break;
						}else if ((cmd.getStartFloor() == downStops[n] && cmd.getState() == WAITING)
								&& canMakeDownStopAt(i)){
							stopAt = downStops[n];
							downIndex = n;
							found = true;
							break;
						}
					}
				}

			}
		}
		
		if (!found){
			stopAt = -1;
		}
	}
	
	private void setUpStopAtNext() {
		boolean found = false;
		if (upIndex < upStops.length){
			for (int n=0; n<upStops.length && !found; n++){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					
					if (cmd.isMoveUp()){ 
						if(cmd.getEndFloor() == upStops[n] && cmd.getState() == LOADED){
							stopAt = upStops[n];
							upIndex = n;
							found = true;
							break;
						}else if (cmd.getStartFloor() == upStops[n] && cmd.getState() == WAITING 
								&& canMakeUpStopAt(i) ){
							stopAt = upStops[n];
							upIndex = n;
							found = true;
							break;
						}
					}
				}
			}
		}
		if (!found){
			stopAt = -1;
		}
	}
	
	private boolean canMakeDownStopAt(int cmdIndex) {
		if (headCount == 0){
			return true;
		}else{
			CommandBean waitCmd =  commandBeanList.get(cmdIndex);
			if (!waitCmd.isMoveUp()){
				for(int i=0; i < commandBeanList.size(); i++){
					if (i != cmdIndex) {
						CommandBean cmd = commandBeanList.get(i);
						if (!cmd.isMoveUp() && cmd.getState() == LOADED 
								&& Math.abs(i-cmdIndex) == 1
								&& (waitCmd.getStartFloor() < cmd.getStartFloor())){
							return true;
						}
					}
				}
			}
			return false;
		}
	}	
	private boolean canMakeUpStopAt(int cmdIndex) {
		if (headCount == 0){
			return true;
		}else{
			CommandBean waitCmd =  commandBeanList.get(cmdIndex);
			if (waitCmd.isMoveUp()){
				for(int i=0; i < commandBeanList.size(); i++){
					if (i != cmdIndex) {
						CommandBean cmd = commandBeanList.get(i);
						if (cmd.isMoveUp() && cmd.getState() == LOADED 
								&& Math.abs(i-cmdIndex) == 1
								&& (waitCmd.getStartFloor() >= cmd.getStartFloor())){
							return true;
						}
					}
				}				
			}
		}
		return false;
	}

	private void initialize(){		
		Set<Integer> ups = new TreeSet<Integer>();
		Set<Integer> downs = new TreeSet<Integer>();
		
		for (int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (cmd.isMoveUp()){
				ups.add(cmd.getStartFloor());
				ups.add(cmd.getEndFloor());
			}else{
				downs.add(cmd.getStartFloor());
				downs.add(cmd.getEndFloor());
			}
		}
		upStops = ups.toArray(new Integer[ups.size()]);
		downStops = downs.toArray(new Integer[downs.size()]);
		//counters
		upIndex = 0;
		downIndex = downStops.length -1;
						
		stopAt = -1;
		headCount = 0;
		boolStarted = false;
		changeDirection = false;
		
		if (logger.isDebugEnabled()){
			logger.debug("upStops=" + Arrays.toString(upStops));
			logger.debug("downStops=" + Arrays.toString(downStops));
		}
	}
	
	private void trackInitCount(boolean elevGoUp) {
		for(int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (elevatorInitPostion == cmd.getStartFloor()
				&& elevGoUp == boolGoUp
				&& cmd.isMoveUp() == boolGoUp
				&& cmd.getState() == WAITING){
				cmd.setState(LOADED);
				headCount++;
			}
		}
		if (logger.isDebugEnabled()){
			logger.debug("INIT ASK" + (boolGoUp?"UP":"DOWN") 
					+ "-->position=" + elevatorInitPostion + ", headCount=" + headCount
					+ ", upIndex=" + upIndex + ", downIndex=" + downIndex);
		}
	}

	private void trackHeadCount() {
		for(int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (stopAt == cmd.getStartFloor()
				&& cmd.getState() == WAITING
				&& boolGoUp == cmd.isMoveUp()){
				if (headCount == 0 ||(headCount > 0 && !changeDirection) ) {
					int nextWaitIndex = findNextWaitIndex();
					CommandBean nextWaitCmd = commandBeanList.get(nextWaitIndex);
					if (nextWaitCmd.isMoveUp() == boolGoUp){
						cmd.setState(LOADED);
						headCount++;
					}else{
						changeDirection = true;
					}
				}
			}else if (stopAt == cmd.getEndFloor()
					&& cmd.getState() == LOADED
					&& boolGoUp == cmd.isMoveUp()){
				cmd.setState(DONE);
				headCount--;
			}
		}
		if (logger.isDebugEnabled()){
			logger.debug("NEXT " + (boolGoUp?"UP":"DOWN") 
					+ "-->stopAt=" + stopAt + ", headCount=" + headCount
					+ ", upIndex=" + upIndex + ", downIndex=" + downIndex
					+ ", changeDirection=" + changeDirection);
		}
		
	}

}

