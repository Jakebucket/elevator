package gfx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import gfx.command.CommandSetDO;
import gfx.command.CommandSetManager;
import gfx.command.CommandSetParser;
import gfx.elevator.ElevatorController;
import gfx.elevator.ModeType;

@Service("elevatorService")
public class ElevatorServiceImpl implements ElevatorService{
	private final static Logger logger = Logger.getLogger(ElevatorServiceImpl.class);
	private String fileName; 
	private ModeType mode;
	
	public ElevatorServiceImpl() {
	}
	public String getFileName() {
		return fileName;
	}
	public ModeType getMode() {
		return mode;
	}
	
	public void process(String fileName, ModeType mode) {
		this.fileName = fileName;
		this.mode = mode;
		
		try {
			//parse commands file
			List<CommandSetDO> commandSetList = CommandSetParser.parse(fileName);
			//prepare command set manager/controller to feed commands
			CommandSetManager commandSetManager = (CommandSetManager) AppContext.getContext().getBean("commandSetManager");
			commandSetManager.setCommandSetList(commandSetList);
			//prepare elevator controller
			ElevatorController elevatorController = (ElevatorController)AppContext.getContext().getBean("elevatorController");
			elevatorController.setMode(mode);
			//process commands and record results
			List<String> results = execute(commandSetManager,elevatorController );
			
			//display results
			showResults(results);
			
		} catch (AppException ex) {
			logger.error("process() failed to parse fileName: " + fileName, ex);
		}
	}
	
	private List<String> execute(CommandSetManager commandSetManager, ElevatorController elevatorController) {
		List<String> results = new ArrayList<String>();
		CommandSetDO commandSetDO = commandSetManager.getNext();
		StringBuilder sb = new StringBuilder();
		sb.append(commandSetDO.getPosition() + " ");
		while (commandSetDO != null) {
			try {
				Integer[] steps = elevatorController.process(commandSetDO);
				int totalDistance = elevatorController.getTotalDistance();
				//sb.append(Arrays.toString(steps));
				sb.append(convertArrayToString(steps));
				sb.append( "(" + totalDistance + ")\n");
				
				commandSetDO = commandSetManager.getNext();
			} catch (AppException ex) {
				sb.append(commandSetDO.toStr() + "(failed)\n");
				logger.error("execute() failed to process commandSetDO: " + commandSetDO.toStr(), ex);
			}
		}
		results.add(sb.toString());
		return results;
	}
	
	private String convertArrayToString(Integer[] steps) {
		return (Arrays.toString(steps)).replaceAll("[\\[,\\]]", " ");
	}
	
	private void showResults(List<String> results) {
		String markLine = "***************************************************\n";
		StringBuilder display = new StringBuilder();
		display.append("\n" +  markLine);
		display.append(mode  + " Results:\n\n");
		for (int i=0;i<results.size();i++){
			display.append(results.get(i) + "\n");
		}
		display.append(markLine);
		
		if (logger.isInfoEnabled()){
			logger.info(display.toString());		
		}else{
			System.out.println(display.toString());		
		}
	}

}
