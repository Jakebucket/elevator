package gfx;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppContext {
	private static ApplicationContext context = null;
	private AppContext() {
	}
	
	static {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
	}
	
	public static ApplicationContext getContext(){
		return context;
	}
}
