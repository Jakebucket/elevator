package gfx.command;

import java.util.List;

import org.apache.log4j.Logger;

public class DispatcherModeA implements Dispatcher{
	private final static Logger logger = Logger.getLogger(DispatcherModeA.class);	
	private int elevatorInitPostion;
	private List<CommandBean>commandBeanList;
	private int commandBeanIndex = -1;
	private int stopAt = -1;
	public DispatcherModeA()  {
		// TODO Auto-generated constructor stub
	}

	public void doCommandSetDO(CommandSetDO commandSetDO) {
		commandBeanList = commandSetDO.getCommandList();
		elevatorInitPostion = commandSetDO.getPosition();
		commandBeanIndex = 0;
	}

	public int getNextStop() {
		if (commandBeanIndex <commandBeanList.size()){
			CommandBean cmd = commandBeanList.get(commandBeanIndex);
			if (stopAt == -1 ){//first
				stopAt = cmd.getStartFloor();
			}else if (stopAt == cmd.getStartFloor()){//start one cmd. move to start
				stopAt = cmd.getEndFloor();
			}else if (stopAt == cmd.getEndFloor()){//finish one cmd, move to next
				commandBeanIndex++;
				if (commandBeanIndex <commandBeanList.size()){
					cmd = commandBeanList.get(commandBeanIndex);
					stopAt = cmd.getStartFloor();
				}else{
					stopAt = -1;
				}
			}
		}else{//done
			stopAt = -1;
		}
		return stopAt;
	}

}
