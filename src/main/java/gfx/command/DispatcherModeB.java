package gfx.command;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import gfx.AppException;
import static gfx.command.CommandStateType.*;
/**
 * Mode B method to detemine where elevator to stop next. -1 means done
 * @author Jake
 *
 */
public class DispatcherModeB implements Dispatcher{
	private final static Logger logger = Logger.getLogger(DispatcherModeB.class);	
	private List<CommandBean>commandBeanList;
	private int elevatorInitPostion;
	//stopAt - for getNextStop
	private int stopAt = -1;
	//working vars - better be defined as a separate class
	private Integer [] upStops;// unique upstops
	private Integer [] downStops; // unique downstops
	private int upIndex = -1;
	private int downIndex = -1;
	//local counters and flags
	private boolean boolStarted = false;
	private boolean boolGoUp = false;
	private boolean changeDirection = false;
	private int headCount = 0;
	
	public DispatcherModeB()  {
		// TODO Auto-generated constructor stub
	}

	public void doCommandSetDO(CommandSetDO commandSetDO){
		elevatorInitPostion = commandSetDO.getPosition(); 
		commandBeanList = commandSetDO.getCommandList();
		initialize();
	}
	
	/**
	 * find next stop -1 means done
	 */
	public int getNextStop() {
		if (!boolStarted){//first time
			setFirstStopAt();
		}else{//already started operation
			if (boolGoUp){
				if (headCount > 0){// continue up
					changeDirection = false;
					setUpStopAtNext();
				}else{//elevator is empty now. where to go
					setEmptyStopAtNext();
				}
			}else{//down
				if (headCount > 0){// continue DOWN
					changeDirection = false;
					setDownStopAtNext();
				}else{//Empty now
					setEmptyStopAtNext();
				}
			}
		}
		trackHeadCount();
		return stopAt;
	}
	
	/**
	 * first stop is based on startFloor for the first commandBean/pair
	 */
	private void setFirstStopAt() {
		CommandBean commandBean = commandBeanList.get(0);
		stopAt = commandBean.getStartFloor();
		boolGoUp = commandBean.isMoveUp();
		boolean elevGoUp = (commandBean.getStartFloor() > elevatorInitPostion);
		boolStarted = true;
		trackInitCount(elevGoUp);
		//any stop needed between

		if (boolGoUp && elevGoUp){
			boolean found = false;
			for (int n=0; n<upStops.length && !found; n++){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (cmd.isMoveUp() 
							&& (cmd.getStartFloor() == upStops[n] && cmd.getState() == WAITING)) {
						stopAt = upStops[n];
						upIndex = n;
						found = true;
						break;
					}
				}
			}
		}else if (!boolGoUp && !elevGoUp){//down
			boolean found = false;
			for (int n= (downStops.length -1); n >= 0 && !found; n--){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (!cmd.isMoveUp() 
							&& (cmd.getStartFloor() == downStops[n] && cmd.getState() == WAITING)) {
						stopAt = downStops[n];
						downIndex = n;
						found = true;
						break;
					}
				}
			}			
		}
		
	}

	private void setEmptyStopAtNext() {
		if (upIndex < upStops.length && downIndex > 0) {
			//boolean nextGoUp = (upStops[upIndex] > downStops[downIndex]);
			if ((upStops[upIndex] == downStops[downIndex])){
				boolGoUp = !boolGoUp; //just change direction
			}else {
				//who is still waiting 
				CommandBean waitCmd = findNextWait();
				if (waitCmd != null) {
					//boolean nextGoUp = (upStops[upIndex] > downStops[downIndex]);
					boolean nextGoUp =(waitCmd.getStartFloor() > stopAt);
					if (nextGoUp == boolGoUp){//same direction
						changeDirection = false;
						if (boolGoUp){
							setUpStopAtNext();
						}else{
							setDownStopAtNext();
						}
					}else{//change direction - counter no move
						changeDirection = true;
						if (nextGoUp) {
							setUpStopAtNext();
						}else{
							setDownStopAtNext();
						}
						boolGoUp = nextGoUp;
					}					
				}else {//no more
					stopAt = -1;
				}
			}
		}else if (upIndex < upStops.length){
			changeDirection = !boolGoUp;
			boolGoUp = true;
			upIndex = 0;
			setUpStopAtNext();
		}else if ( downIndex > 0){
			changeDirection = !boolGoUp;
			boolGoUp = false;
			downIndex = upStops.length -1;
			setDownStopAtNext();
		}else{
			stopAt = -1;
		}
		
	}

	private CommandBean findNextWait() {
		for(int i=0;i<commandBeanList.size();i++){
			if (commandBeanList.get(i).getState() == WAITING){
				return commandBeanList.get(i);
			}
		}
		return null;
	}
	private int findNextWaitIndex() {
		for(int index=0;index <commandBeanList.size();index++){
			if (commandBeanList.get(index).getState() == WAITING){
				return index;
			}
		}
		return -1;
	}

	private void setDownStopAtNext() {
		boolean found = false;
		if (downIndex >= 0){
			for (int n= downStops.length-1; n >= 0 && !found; n--){

				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					if (!cmd.isMoveUp()){
						if ((cmd.getEndFloor() == downStops[n] && cmd.getState() == LOADED)){
							stopAt = downStops[n];
							downIndex = n;
							found = true;
							break;
						}else if ((cmd.getStartFloor() == downStops[n] && cmd.getState() == WAITING)
								&& canMakeDownStopAt(i)){
							stopAt = downStops[n];
							downIndex = n;
							found = true;
							break;
						}
					}
				}

			}
		}
		
		if (!found){
			stopAt = -1;
		}
	}
	
	private void setUpStopAtNext() {
		boolean found = false;
		if (upIndex < upStops.length){
			for (int n=0; n<upStops.length && !found; n++){
				for(int i=0;i<commandBeanList.size();i++){
					CommandBean cmd = commandBeanList.get(i);
					
					if (cmd.isMoveUp()){ 
						if(cmd.getEndFloor() == upStops[n] && cmd.getState() == LOADED){
							stopAt = upStops[n];
							upIndex = n;
							found = true;
							break;
						}else if (cmd.getStartFloor() == upStops[n] && cmd.getState() == WAITING 
								&& canMakeUpStopAt(i) ){
							stopAt = upStops[n];
							upIndex = n;
							found = true;
							break;
						}
					}
				}
			}
		}
		if (!found){
			stopAt = -1;
		}
	}
	
	private boolean canMakeDownStopAt(int cmdIndex) {
		if (headCount == 0){
			return true;
		}else{
			CommandBean waitCmd =  commandBeanList.get(cmdIndex);
			if (!waitCmd.isMoveUp()){
				for(int i=0; i < commandBeanList.size(); i++){
					if (i != cmdIndex) {
						CommandBean cmd = commandBeanList.get(i);
						if (!cmd.isMoveUp() && cmd.getState() == LOADED 
								&& Math.abs(i-cmdIndex) == 1
								&& (waitCmd.getStartFloor() < cmd.getStartFloor())){
							return true;
						}
					}
				}
			}
			return false;
		}
	}	
	private boolean canMakeUpStopAt(int cmdIndex) {
		if (headCount == 0){
			return true;
		}else{
			CommandBean waitCmd =  commandBeanList.get(cmdIndex);
			if (waitCmd.isMoveUp()){
				for(int i=0; i < commandBeanList.size(); i++){
					if (i != cmdIndex) {
						CommandBean cmd = commandBeanList.get(i);
						if (cmd.isMoveUp() && cmd.getState() == LOADED 
								&& Math.abs(i-cmdIndex) == 1
								&& (waitCmd.getStartFloor() >= cmd.getStartFloor())){
							return true;
						}
					}
				}				
			}
		}
		return false;
	}

	private void initialize(){		
		Set<Integer> ups = new TreeSet<Integer>();
		Set<Integer> downs = new TreeSet<Integer>();
		
		for (int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (cmd.isMoveUp()){
				ups.add(cmd.getStartFloor());
				ups.add(cmd.getEndFloor());
			}else{
				downs.add(cmd.getStartFloor());
				downs.add(cmd.getEndFloor());
			}
		}
		upStops = ups.toArray(new Integer[ups.size()]);
		downStops = downs.toArray(new Integer[downs.size()]);
		//counters
		upIndex = 0;
		downIndex = downStops.length -1;
						
		stopAt = -1;
		headCount = 0;
		boolStarted = false;
		changeDirection = false;
		
		if (logger.isDebugEnabled()){
			logger.debug("upStops=" + Arrays.toString(upStops));
			logger.debug("downStops=" + Arrays.toString(downStops));
		}
	}
	
	private void trackInitCount(boolean elevGoUp) {
		for(int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (elevatorInitPostion == cmd.getStartFloor()
				&& elevGoUp == boolGoUp
				&& cmd.isMoveUp() == boolGoUp
				&& cmd.getState() == WAITING){
				cmd.setState(LOADED);
				headCount++;
			}
		}
		if (logger.isDebugEnabled()){
			logger.debug("INIT ASK" + (boolGoUp?"UP":"DOWN") 
					+ "-->position=" + elevatorInitPostion + ", headCount=" + headCount
					+ ", upIndex=" + upIndex + ", downIndex=" + downIndex);
		}
	}

	private void trackHeadCount() {
		for(int i=0;i<commandBeanList.size();i++){
			CommandBean cmd = commandBeanList.get(i);
			if (stopAt == cmd.getStartFloor()
				&& cmd.getState() == WAITING
				&& boolGoUp == cmd.isMoveUp()){
				if (headCount == 0 ||(headCount > 0 && !changeDirection) ) {
					int nextWaitIndex = findNextWaitIndex();
					CommandBean nextWaitCmd = commandBeanList.get(nextWaitIndex);
					if (nextWaitCmd.isMoveUp() == boolGoUp){
						cmd.setState(LOADED);
						headCount++;
					}else{
						changeDirection = true;
					}
				}
			}else if (stopAt == cmd.getEndFloor()
					&& cmd.getState() == LOADED
					&& boolGoUp == cmd.isMoveUp()){
				cmd.setState(DONE);
				headCount--;
			}
		}
		if (logger.isDebugEnabled()){
			logger.debug("NEXT " + (boolGoUp?"UP":"DOWN") 
					+ "-->stopAt=" + stopAt + ", headCount=" + headCount
					+ ", upIndex=" + upIndex + ", downIndex=" + downIndex
					+ ", changeDirection=" + changeDirection);
		}
		
	}

}
