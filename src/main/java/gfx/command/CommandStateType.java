package gfx.command;

public enum CommandStateType {
	WAITING(-1),
	LOADED(1),
	DONE(0);
	
	private int value;
	private CommandStateType(int value){
		this.value = value;
	}
}
