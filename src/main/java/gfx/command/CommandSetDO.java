package gfx.command;

import java.util.ArrayList;
import java.util.List;

public class CommandSetDO {
	private int position;
	private List<CommandBean> commandList = new ArrayList<CommandBean>();
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public List<CommandBean> getCommandList() {
		return commandList;
	}
	public void setCommandList(List<CommandBean> commandList) {
		this.commandList = commandList;
	}
	
	/**
	 * validate command data 
	 * @return
	 */
	public boolean validate(){
		return (position > 0 && commandList.size() > 0);
	}
	
	/**
	 * format command set for output
	 * @return
	 */
	public String toStr(){
		StringBuilder sb = new StringBuilder();
		sb.append(position);
		sb.append(" ");
		for (int i=0;i<commandList.size();i++){
			sb.append(commandList.get(i).getStartFloor());
			sb.append(" ");
			sb.append(commandList.get(i).getEndFloor());
			sb.append(" ");
		}
		
		return sb.toString();
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("CommandSetDO \n[position=" + position);
		sb.append(", commandList=");
		for (int i=0;i<commandList.size();i++){
			sb.append("(");
			sb.append(commandList.get(i).getStartFloor() + ", ");
			sb.append(commandList.get(i).getEndFloor());
			sb.append(") ");
		}
		sb.append("]");
		return sb.toString();
	}
	
	
}
