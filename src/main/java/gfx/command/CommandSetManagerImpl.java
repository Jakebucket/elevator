package gfx.command;

import java.util.ArrayList;
import java.util.List;
/**
 * provide next CommandSetDO available 
 * @author Jake
 *
 */

public class CommandSetManagerImpl implements CommandSetManager{
	private List<CommandSetDO> commandSetList = new ArrayList<CommandSetDO>();
	private int index; //keep pointer for next CommandSetDO

	public void setCommandSetList(List<CommandSetDO> commandSetList) {
		this.commandSetList = commandSetList;
	}
	
	public CommandSetDO getNext(){
		if (index < commandSetList.size()){
			return commandSetList.get(index++);
		}else{
			return null;//no more left
		}
	}
	
}
