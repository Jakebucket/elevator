package gfx.command;

import gfx.AppException;

public interface Dispatcher {
	void doCommandSetDO(CommandSetDO commandSet) throws AppException;
	int getNextStop();
}
