package gfx.command;

public class CommandBean {
	private int startFloor;
	private int endFloor;
	private boolean moveUp;
	private CommandStateType state = CommandStateType.WAITING;
	
	public CommandBean(){}
	
	public CommandBean(int start, int end){
		this.startFloor = start;
		this.endFloor = end;
		this.moveUp = (end > start);
	}
	
	public int getStartFloor() {
		return startFloor;
	}
	
	public void setStartFloor(int startFloor) {
		this.startFloor = startFloor;
	}
	public int getEndFloor() {
		return endFloor;
	}
	public void setEndFloor(int endFloor) {
		this.endFloor = endFloor;
	}
	public boolean isMoveUp() {
		return moveUp;
	}
	public void setMoveUp(boolean moveUp) {
		this.moveUp = moveUp;
	}

	public CommandStateType getState() {
		return state;
	}

	public void setState(CommandStateType state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "CommandBean [startFloor=" + startFloor + ", endFloor=" + endFloor + ", moveUp=" + moveUp + ", state="
				+ state + "]";
	}
	
}
