package gfx.command;

import java.util.List;

public interface CommandSetManager {
	void setCommandSetList(List<CommandSetDO> commandSetList);
	CommandSetDO getNext();
}
