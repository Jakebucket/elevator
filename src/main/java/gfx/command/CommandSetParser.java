package gfx.command;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import gfx.AppException;

/**
 * Util class - parse input file to build CommandSet list
 * @author Jake
 *
 */
public class CommandSetParser {
	private final static Logger logger = Logger.getLogger(CommandSetParser.class);
		
	public static List<CommandSetDO> parse(String fileName)throws AppException{
		List<CommandSetDO> commandSetList = new ArrayList<CommandSetDO>();
		BufferedReader reader = null;
		
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine();
			while (line != null) {
				commandSetList.add(parseLine(line));
				line = reader.readLine();
			}
		} catch (FileNotFoundException e) {
			String err = "Failed to find the file: " + fileName;
			logger.error(err + e.getMessage());
			throw new AppException(e);
		} catch (IOException e) {
			String err = "Failed to read the file: " + fileName;
			logger.error(err + e.getMessage());
			throw new AppException(e);
		}finally{
			if (reader !=null){
				try {
					reader.close();
				} catch (IOException e) {
					logger.warn("Unable to close the file " + fileName + e.getMessage());				
				}
			}
		}
		
		return commandSetList;
	}

	private static CommandSetDO parseLine(String line) {
		CommandSetDO commandSet = null; 
		String [] data = line.split(":");
		if (data.length == 2){
			commandSet = new CommandSetDO();
			commandSet.setPosition(Integer.parseInt(data[0]));
			String[] commands = data[1].split(",");
			if (commands.length > 0) {
				for (int i=0;i <commands.length;i++){
					String[] cmd = commands[i].split("-");
					if (cmd.length == 2){
						commandSet.getCommandList().add(
								new CommandBean(Integer.parseInt(cmd[0]), Integer.parseInt(cmd[1])));
					}else{
						logger.warn("Unable to parse command: " + commands[i]);
					}
				}
			}else{
				logger.warn("Unable to parse data commands: " + data[1]);
			}
		}else{
			logger.warn("Unable to parse data line: " + line);
		}
		return commandSet;
	}
	
	
}
