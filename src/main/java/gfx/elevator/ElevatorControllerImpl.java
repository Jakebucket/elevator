package gfx.elevator;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import gfx.AppContext;
import gfx.AppException;
import gfx.command.CommandSetDO;
import gfx.command.Dispatcher;

public class ElevatorControllerImpl implements ElevatorController{
	private final static Logger logger = Logger.getLogger(ElevatorControllerImpl.class);
	private ModeType mode;
	private int totalDistance;
	public ElevatorControllerImpl() {
		// TODO Auto-generated constructor stub
	}

	public void setMode(ModeType mode) {
		this.mode = mode;
	}

	/**
	 * Dispatcher find next stop from CommandSetDO, and
	 * Tell Elevator to move to next stop.
	 * After finished, get total distance from elevator
	 * @return result Integer[] 
	 * @throws AppException
	 */
	public Integer[] process(CommandSetDO commandSetDO) throws AppException {
		if (validate(commandSetDO)){
			List <Integer> stepsList =new ArrayList<Integer>(); 
			logger.debug("process commandSetDO: " + commandSetDO.toString());
			Elevator elevator = (Elevator) AppContext.getContext().getBean("elevator");
			elevator.initialize(commandSetDO.getPosition());
			Dispatcher dispatcher = getDispatcher();
			dispatcher.doCommandSetDO(commandSetDO);
			int nextStop = dispatcher.getNextStop();
			while(nextStop > 0 ) {
				stepsList.add(nextStop);
				elevator.moveTo(nextStop);
				nextStop = dispatcher.getNextStop();
			}
			totalDistance = elevator.getTotalDistance();
			
			return stepsList.toArray(new Integer[stepsList.size()]);
		}else{
			String err = "invalid CommandSetDO to process.";
			logger.error(err);
			throw new AppException(err);
		}
	}
	
	/**
	 * There are to dispatchers based on mode A or B
	 * @return Dispatcher
	 * @throws AppException
	 */
	private Dispatcher getDispatcher() throws AppException{
		if (mode ==ModeType.A_MODE){
			return (Dispatcher) AppContext.getContext().getBean("dispatcherModeA");
		}else if (mode ==ModeType.B_MODE){
			return (Dispatcher) AppContext.getContext().getBean("dispatcherModeB");
		}else{
			throw new AppException("Unknow mode to load dispatcher.");
		}
	}

	private boolean validate(CommandSetDO commandSetDO){
		return (commandSetDO != null 
				&& commandSetDO.getPosition() > 0
				&& commandSetDO.getCommandList().size() > 0);
	}

	public int getTotalDistance() {
		return totalDistance;
	}

}
