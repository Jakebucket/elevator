package gfx.elevator;

import gfx.AppException;
import gfx.command.CommandSetDO;

public interface ElevatorController {
	void setMode(ModeType mode);
	Integer[] process(CommandSetDO commandSetDO) throws AppException;
	int getTotalDistance();
}
