package gfx.elevator;

public interface Elevator {
	void initialize(int position);
	int getPosition();
	int getTotalDistance();
	void moveTo(int nextStop);
}
