package gfx.elevator;

public enum ModeType {
	A_MODE("A"),
	B_MODE("B"),
	UNKNOWN("X");
	
	private String mode;
	
	private ModeType(String mode){
		this.mode = mode;
	}
	
	public String getName(){
		return mode;
	}
	
	public static ModeType getMode(String mode){
		if (A_MODE.getName().equals(mode)){
			return A_MODE;
		}else if (B_MODE.getName().equals(mode)){
			return B_MODE;
		}else{
			return UNKNOWN;
		}
	}
}
