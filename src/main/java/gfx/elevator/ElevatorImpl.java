package gfx.elevator;

public class ElevatorImpl implements Elevator {
	private int position;
	private int totalDistance;
	
	public ElevatorImpl() {
		
	}

	public int getPosition() {
		return position;
	}

	public void initialize(int position) {
		this.position = position;
		this.totalDistance = 0;
	}

	public int getTotalDistance() {
		return totalDistance;
	}

	public void moveTo(int nextStop) {
		totalDistance += Math.abs(position - nextStop);
		position = nextStop;
	}

	

}
