package gfx;

import gfx.elevator.ModeType;

public interface ElevatorService {
	void process(String fileName, ModeType mode);
}
