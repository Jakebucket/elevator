package gfx;

import org.apache.log4j.Logger;

import gfx.elevator.ModeType;

public class AppMain {
	private final static Logger logger = Logger.getLogger(AppMain.class);
	
	
	public static void main(String[] args) {
		if (validate(args)){
			ElevatorService service = (ElevatorService) AppContext.getContext().getBean("elevatorService");
			service.process(args[0], ModeType.getMode(args[1]));
		}else{
			System.exit(1);
		}
	}
	
	private static boolean validate(String[] args){
		if (args.length != 2) {
			logger.error("Please input two parameters to run the program: fileName and mode");
			return false;
		}else if (ModeType.getMode(args[1]).equals(ModeType.UNKNOWN)){
			logger.error("Un-defined mode: " + args[1]);
			return false;
		}else{
			return true;
		}
		
	}
}
